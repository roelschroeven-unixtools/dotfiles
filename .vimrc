" 
" My vimrc file (Roel Schroeven roel@roelschroeven.net)
"
" based on an example by Bram Moolenaar plus personal customizations.
"

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set nobackup		" do not keep a backup file, use versions instead
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" This is an alternative that also works in block mode, but the deleted
" text is lost and it only works for putting the current register.
"vnoremap p "_dp

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  " set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

" Personal stuff
:filetype on
:autocmd FileType c,cpp,javascript :set cindent
:autocmd FileType c,cpp,javascript :set foldmethod=indent
:autocmd FileType c,cpp,javascript :set foldcolumn=4
:autocmd FileType c,cpp,javascript :set foldlevel=999
:autocmd FileType c,cpp,javascript :set textwidth=79
:autocmd FileType c,cpp,javascript :set formatoptions=rqwn
:set linebreak
:set shiftwidth=4
:set softtabstop=4
:set expandtab
:set cinoptions={1s,l1,g0,(1s,\:1s,i1s,W1s
:set showmatch
:set showfulltag
:set visualbell
:set nohlsearch
:set incsearch
:set laststatus=2
:set nowrap
:set hidden
:set nobackup
:set writebackup
:set autoread

:set background=dark

:set viminfo='1000,f1,<500

":colorscheme professional
if has("gui_running")
  ":set guifont=Andale\ Mono\ 8
  :set lines=50
  :set columns=100
endif

" Open files at last edited location
:autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal g'\"" | endif

" Change current directory to directory of current file
:autocmd BufEnter * cd %:p:h

" Single-key toggle paste mode
" (see https://vim.fandom.com/wiki/Toggle_auto-indenting_for_code_paste)
" Enable paste mode: enter insert mode, then press the configured key
" Disable paste mode: press the key again, while still in insert mode
" TODO: doesn't seem to work in a screen session :-(
set pastetoggle=<F2>

" Source python.vim
" :au FileType python source ~/.vim/scripts/python.vim
" You can set the global variable "g:py_select_leading_comments" to 0
" if you don't want to select comments preceding a declaration (these
" are usually the description of the function/class).
" You can set the global variable "g:py_select_trailing_comments" to 0
" if you don't want to select comments at the end of a function/class.
" If these variables are not defined, both leading and trailing comments
" are selected.
" Example: (in your .vimrc) "let g:py_select_leading_comments = 0"
" You may want to take a look at the 'shiftwidth' option for the
" shift commands...
