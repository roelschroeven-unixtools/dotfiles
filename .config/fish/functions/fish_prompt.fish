# Set the default prompt command. Make sure that every terminal escape
# string has a newline before and after, so that fish will know how
# long it is.

function fish_prompt --description "Write out the prompt"

    #
    # Just calculate these once, to save a few cycles when displaying the prompt
    #

    # Hostname
    if not set -q __fish_prompt_hostname
        set -g __fish_prompt_hostname (hostname|cut -d . -f 1)
    end

    # Normal color
    if not set -q __fish_prompt_normal
        set -g __fish_prompt_normal (set_color normal)
    end

    # Color for user@hostname
    if not set -q __prompt_user_color
        set -g __prompt_user_color (set_color -o green)
    end

    # Grey color
    if not set -q __prompt_grey
        set -g __prompt_grey (set_color 999)
    end

    switch $USER
        case root
            set prompt_char '#'
            # cwd color for root fish_color_cwd_root if set, otherwise fish_color_cwd
            if not set -q __fish_prompt_cwd
                if set -q fish_color_cwd_root
                    set -g __fish_prompt_cwd (set_color $fish_color_cwd_root)
                else
                    set -g __fish_prompt_cwd (set_color $fish_color_cwd)
                end
            end
        case '*'
            set prompt_char '$'
            # cwd color for normal users
            if not set -q __fish_prompt_cwd
                set -g __fish_prompt_cwd (set_color $fish_color_cwd)
            end
    end

    echo -n -s \n "$__prompt_user_color" "$USER" @ "$__fish_prompt_hostname" ' ' "$__fish_prompt_cwd" (prompt_pwd_long) \
        \n "$__prompt_grey" "[fish]$__fish_prompt_normal" " $prompt_char "
end

